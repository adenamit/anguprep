import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Feedback, ContactType} from '../shared/feedback';
// import {flyInOut, visibility, expand} from '../animations/app.animation';
import {Params, ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { MatCard } from '@angular/material';


@Component({
  selector: 'app-mecard',
  templateUrl: './mecard.component.html',
  styleUrls: ['./mecard.component.css']//,
//   host : {
//     '[@flyInOut]': 'true',
//     'style': 'display: block;'
// },
// animations: [
//   flyInOut(),
//   visibility(),
//   expand()
// ]
})



export class MecardComponent implements OnInit {

errMess:string;
feedbackForm: FormGroup;
feedback: Feedback;
fbcopy: Feedback;
contactType = ContactType;
postReturn:number;
visibility:string;
vis:boolean;
res:boolean;

formErrors= {
'firstname':'',
'lastname': '',
'telnum': '',
'email': '',
};

validationMessages={
'firstname': {
'required': 'First Name is required',
'minlength': 'First Name must be at least 2 characters long',
'maxlength': 'First Name cannot be more than 25 characters long'
},
'lastname': {
'required': 'Last Name is required',
'minlength': 'Last Name must be at least 2 characters long',
'maxlength': 'Last Name cannot be more than 25 characters long'
},
'telnum': {
  'required': 'Tel. Number is required',
  'pattern': 'Tel. number must contain only numbers.'
},
'email': {
  'required': 'Email is required',
  'email': 'Email not in valid format'
}

}

  constructor(private fb: FormBuilder,  private route: ActivatedRoute ) {
    

    this.createForm();
  }

  ngOnInit() {
    this.vis=true;
    this.res=false;
  }

  createForm(){
this.feedbackForm = this.fb.group({
firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ] ],
lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ]],
telnum: [0, [Validators.required, Validators.pattern ]],
email: ['', [Validators.required, Validators.email ] ] ,
agree: false,
contacttype: 'None',
message: '',
});

this.feedbackForm.valueChanges.subscribe(data=> this.onValueChanged(data) );
this.onValueChanged(); // (re)set form validation messages 
  }

  onValueChanged(data?: any){
   
  }

  miStuff():boolean{
  return true;
  }
  onSubmit(){
       
  }

 
}

//https://scotch.io/tutorials/responsive-equal-height-with-angular-directive

//https://scotch.io/tutorials/angular-2-transclusion-using-ng-content
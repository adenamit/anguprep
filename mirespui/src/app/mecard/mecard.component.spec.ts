import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MecardComponent } from './mecard.component';

describe('MecardComponent', () => {
  let component: MecardComponent;
  let fixture: ComponentFixture<MecardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MecardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MecardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

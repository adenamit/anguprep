import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
 
 import { HealthListComponent }    from './hero-list.component';
// import { HeroDetailComponent }  from './health-details/health-detail.component';
 
import { HealthDetailService } from './hservice/healthdetail.service';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    // HeroListComponent,
    // HeroDetailComponent
  ],
  providers: [ HealthDetailService ]
})
export class HeroesModule {}
import { Injectable } from '@angular/core';


import 'rxjs/add/observable/of';

 import { HealthdetailComponent } from '../healthdetail/healthdetail.component';
 import { HealthDetailsComponent } from '../health-details/health-details.component';
 import { HEALTH_DETAIL } from '../hdetail';
import { HEALTH_DETAILS } from '../hdetails';
// import { MessageService } from './message.service';

// @Injectable({
//   providedIn: '',
// })
export class HealthDetailService {



  getHealthDetails(): Observable<HEALTH_DETAIL[]> {
    // TODO: send the message _after_ fetching the heroes
  
    return of(HEALTH_DETAILS);
  }
}
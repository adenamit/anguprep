import { Routes } from '@angular/router';
import { MecardComponent } from '../mecard/mecard.component';
import { HomeComponent } from '../home/home.component';
import { AboutmeComponent } from '../aboutme/aboutme.component';
import { HeaderComponent } from '../header/header.component';



export const routes: Routes = [
{
    path: '', 
    component : HomeComponent
},
{
    path: 'home', 
    component: HomeComponent
},
{
    path: 'myhealth',
    component: /health/HealthDeatils 
},

{
    path: 'myinfo', component : AboutmeComponent
},
{
    path: '**', component : HeaderComponent
}
];







//,
// {
//     path: 'dishdetail/:id', component : DishDetailComponent
// },
// {
//     path: '', redirectTo: '/home', pathMatch: 'full'
// },
// {
// path: 'contactus', component: ContactComponent
// }
import { Directive, ElementRef, Input, NgZone,HostListener } from '@angular/core';
import {gVariables} from '../shared/gvariables';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Directive({
  selector: '[appMenusize]',
})
export class MenusizeDirective implements OnInit {
  lmenu:boolean;
  winHeight:number;
  constructor(private ngZone: NgZone, private gvar:gVariables) {}
  ngOnInit(){
    this.gvar.largeMenu.subscribe(ext_val_lgmenu => this.lmenu = ext_val_lgmenu);
    this.gvar.windowHeight.subscribe(winHeight => this.winHeight = winHeight);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event){

    // console.log("gvar be4:" + this.gvar.largeMenu.toString());
          //ngZone.run will help to run change detection
          this.ngZone.run(() => {
              // console.log("Width: " + window.innerWidth);
              // console.log("Height: " + window.innerHeight);
  
         if(window.innerWidth>768){
this.gvar.changeMenuTypeValue(true);
         }
         else {
          this.gvar.changeMenuTypeValue(false);
         }
          });
          this.gvar.changeWindowHeightValue(window.innerHeight);
          
}

}

import { Component, OnInit,  ViewChild, NgZone, HostListener } from '@angular/core';
import { gVariables } from '../shared/gvariables';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {

 lmenu:boolean;
  constructor(private ngZone: NgZone, private gvar:gVariables ) { } //private gvar:gVariables
  ngOnInit(){
    this.gvar.largeMenu.subscribe(ext_val_lgmenu => this.lmenu = ext_val_lgmenu);
          //ngZone.run will help to run change detection
       

              this.ngZone.run(() => {
                console.log("Width: " + window.innerWidth);
                console.log("Height: " + window.innerHeight);
                console.log("gvar before init:" + this.lmenu);
           if(window.innerWidth>768){
  this.gvar.changeMenuTypeValue(true);
           }
           else {
            this.gvar.changeMenuTypeValue(false);
           }
            });
             console.log("gvar init:" + this.lmenu);
             

      
}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppMeCardComponent } from './app-me-card.component';

describe('AppMeCardComponent', () => {
  let component: AppMeCardComponent;
  let fixture: ComponentFixture<AppMeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppMeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppMeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

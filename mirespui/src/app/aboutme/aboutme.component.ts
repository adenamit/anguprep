import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Feedback, ContactType} from '../shared/feedback';
// import {flyInOut, visibility, expand} from '../animations/app.animation';
import 'rxjs/add/operator/toPromise';
import { MatCard } from '@angular/material';

@Component({
  selector: 'app-aboutme',
  templateUrl: './aboutme.component.html',
  styleUrls: ['./aboutme.component.less']
})
export class AboutmeComponent implements OnInit {


errMess:string;
feedbackForm: FormGroup;
feedback: Feedback;
fbcopy: Feedback;
contactType = ContactType;
postReturn:number;
visibility:string;
vis:boolean;
res:boolean;

formErrors= {
'firstname':'',
'lastname': '',
'telnum': '',
'email': '',
};

validationMessages={
'firstname': {
'required': 'First Name is required',
'minlength': 'First Name must be at least 2 characters long',
'maxlength': 'First Name cannot be more than 25 characters long'
},
'lastname': {
'required': 'Last Name is required',
'minlength': 'Last Name must be at least 2 characters long',
'maxlength': 'Last Name cannot be more than 25 characters long'
},
'telnum': {
  'required': 'Tel. Number is required',
  'pattern': 'Tel. number must contain only numbers.'
},
'email': {
  'required': 'Email is required',
  'email': 'Email not in valid format'
}

}

  constructor(private fb: FormBuilder ) {
    

    this.createForm();
  }

  ngOnInit() {
    this.vis=true;
    this.res=false;
  }

  createForm(){
this.feedbackForm = this.fb.group({
firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ] ],
lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ]],
telnum: [0, [Validators.required, Validators.pattern ]],
email: ['', [Validators.required, Validators.email ] ] ,
agree: false,
contacttype: 'None',
message: '',
});

this.feedbackForm.valueChanges.subscribe(data=> this.onValueChanged(data) );
this.onValueChanged(); // (re)set form validation messages 
  }

  onValueChanged(data?: any){
   
  }

  miStuff():boolean{
  return true;
  }
  onSubmit(){
       
  }

 
}
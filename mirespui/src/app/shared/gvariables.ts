import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable()
export class gVariables {
  private valueSource = new BehaviorSubject(false);
  largeMenu = this.valueSource.asObservable();
 
  changeMenuTypeValue(largeMenu: boolean) {
    this.valueSource.next(largeMenu)
  }



  private windowHeightValueSource = new BehaviorSubject(0);
  windowHeight = this.windowHeightValueSource.asObservable();
 
  changeWindowHeightValue(windowHeight: number) {
    this.windowHeightValueSource.next(windowHeight)
  }

}

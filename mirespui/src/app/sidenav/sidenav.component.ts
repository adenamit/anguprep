import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy, OnInit, Input, NgZone, HostListener} from '@angular/core';
import { gVariables } from '../shared/gvariables';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit, OnDestroy {
  lmenu:boolean;

  fillerNav=[];
  isSidenavOpened:boolean=false;
  constructor( private ngZone: NgZone, private gvar:gVariables) {
 
  }


  ngOnInit() {
    this.gvar.largeMenu.subscribe(ext_val_lgmenu => this.lmenu = ext_val_lgmenu);
    console.log("sidenav State opened on init?  " + this.isSidenavOpened)

    this.fillerNav = Array.from({length: 50}, (_, i) => `Nav Item ${i + 1}`);
    console.log("isMObile?  " + this.lmenu);

    this.ngZone.run(() => {
      console.log("Width: " + window.innerWidth);
      console.log("Height: " + window.innerHeight);
      console.log("gvar before init:" + this.lmenu);
 if(window.innerWidth>768){
  this.gvar.changeMenuTypeValue(true);
 }
 else {
  this.gvar.changeMenuTypeValue(false);
 }
  });
   console.log("gvar init:" + this.lmenu);
  }


  
/** @title Responsive sidenav */


openMi(){
  console.log("sidenav State opened?  " + this.isSidenavOpened)
  if(this.isSidenavOpened){
this.isSidenavOpened=false;}
else{
  this.isSidenavOpened=true;
}

}


  fillerContent = Array.from({length: 50}, () =>
      `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);



  ngOnDestroy(): void {
    console.log("destroy called");
  }

}

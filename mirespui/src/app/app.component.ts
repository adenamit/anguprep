import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy, OnInit, NgZone} from '@angular/core';
import { gVariables } from './shared/gvariables';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  lmenu:boolean;
  winHeight:number;
 constructor( 
  //  changeDetRef: ChangeDetectorRef, mediaMain: MediaMatcher, 
   private gvar:gVariables, private ngZone: NgZone) {
  //  this.mobQuery = mediaMain.matchMedia('(max-width: 600px)');
  //  this._mobQueryListener = () => changeDetRef.detectChanges();
  //  this.mobQuery.addListener(this._mobQueryListener);
 }
//  mobQuery: MediaQueryList;
//  private _mobQueryListener: () => void;
 ngOnInit() {this.gvar.largeMenu.subscribe(ext_val_lgmenu => this.lmenu = ext_val_lgmenu);
  this.gvar.windowHeight.subscribe(winHeight => this.winHeight = winHeight);
        //ngZone.run will help to run change detection
        this.gvar.changeWindowHeightValue(window.innerHeight);
            console.log("Width: " + window.innerWidth);
            console.log("Height: " + window.innerHeight);

            this.ngZone.run(() => {
           
         if(window.innerWidth>768){
this.gvar.changeMenuTypeValue(true);
         }
         else {
          this.gvar.changeMenuTypeValue(false);
         }
          });
console.log("MobMenu? " + this.lmenu);
 }
  title = 'app';

  ngOnDestroy(): void {
    // this.mobQuery.removeListener(this._mobQueryListener);
  }
}
